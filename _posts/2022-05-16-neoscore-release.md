---
layout: post
title: Notation without bars
date: 2022-05-16
public: true
permalink: /blog/neoscore-release
category: thoughts
image: /img/posts/neoscore-release/neoscore_promo_image.png
---

Some 7 years ago I used to be really into making generative music scores, where [a program generates a score](https://f002.backblazeb2.com/file/ayoon-public-files/the_second_hand_excerpt.pdf) unique to each performance. This was a lot of fun, but I hit a terrible wall when I realized there is virtually no software out there capable of engraving unconventional graphic notation, so any music I made with this method had to be fairly well-behaved. In 2016 while I was learning how to program properly at [The Recurse Center](https://www.recurse.com/) I tried to build such a thing - it _kind of_ worked, but getting it to production quality was beyond my skill level. I threw it up on my github page and forgot about it.
  
Fast forward to winter 2021 - I get an email out of the blue from a guy named Craig Vear. Craig has been using my half-baked notation program, and he was wondering how to do a particular thing. I was free, so I dug up the code, blew the proverbial dust off it, and found about 3 bugs making that thing impossible. I was free, so I fixed them and emailed back. It turns out Craig Vear is the head researcher of a huge [EU-funded research project focusing on digital scores](https://digiscore.dmu.ac.uk/), and Professor Vear wants to fund me to revisit and complete the notation program.
  
Fast forward to today - [Neoscore](https://github.com/DigiScore/neoscore) is officially released, and it is pretty cool! Here it is drawing some randomly generated squiggles and colors:
  
[![A score featuring squiggly lines and colored blocks](/img/posts/neoscore-release/neoscore_promo_image.png)](https://github.com/DigiScore/neoscore/blob/main/examples/promo_image.py)

and here it is notating the first page of one of Morton Feldman's graph paper pieces:

[![A score consisting of floating grid cells](/img/posts/neoscore-release/feldman_projection_2.png)](https://github.com/DigiScore/neoscore/tree/main/examples/feldman_projection_2)

While other notation software assumes scores follow a narrow set of rules, neoscore treats scores as shapes and text with as few assumptions as possible. In neoscore, staves and noteheads are just one way of writing. Its programmatic nature makes it especially useful for generative scoremaking, and it even supports animation and live-coding! It's not a visual editor like Sibelius or MuseScore - it's a Python library that requires some chops to operate, but perhaps it can be a step in that direction?
  
Today along with the release **we're announcing a competition for making unusual scores with the library**. The commission winner will get 3000 pounds (great british) and covered travel to the premiere at [TENOR 2023](https://tenor2022.prism.cnrs.fr/) in Boston. If you're interested in this, check out [the full commission description and submission form here](https://digiscore.dmu.ac.uk/neoscore-commision-eoi-portal/). Note that fairly strong programming skills are a requirement for this commission.
  
So [check it out](https://neoscore.org)! Let me know what you think! And if you have a github account, [please star the repo](https://github.com/DigiScore/neoscore) 😅
  
xoxo
~ay
